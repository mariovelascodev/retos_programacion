#Bucle for con un rango de números del 1 al 101
for number in range(1, 101):
    #Si number es múltiplo de 3 y 5 muestra la palabra fizzbuzz
    if number % 3 == 0 and number % 5 == 0:
        print("fizzbuzz")
    #Si solo es múltiplo de 3 muestra la palabra fizz
    elif number % 3 == 0:
        print("fizz")
    #Si solo es múltiplo de 5 muestra a palabra buzz
    elif number % 5 == 0:
        print("buzz")
    #Si no es múltiplo de 3 o de 5 muesta el numero
    else:
        print(number)