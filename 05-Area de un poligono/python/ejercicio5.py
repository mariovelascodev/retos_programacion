def area_polygon(type_polygon):
    """
    Dependiendo del tipo de polígono introducido,
    nos calculara su área, y si no nos dirá cuales son
    los polígonos soportados.
    """
    if type_polygon.lower() == "triangulo":
        height = float(input("Introduce la altura: "))
        base = float(input("Introduce la base: "))
        return f"{base * height / 2} es el área del triángulo"
    elif type_polygon.lower() == "cuadrado":
        side = float(input("Introduce el lado: "))
        return f"{side ** 2} es el área del cuadrado"
    elif type_polygon.lower() == "rectangulo":
        height = float(input("Introduce la altura: "))
        base = float(input("Introduce la base: "))
        return f"{base * height} es el área del rectángulo"
    else:
        return "Los polígonos soportados son triángulo, cuadrado, rectángulo"

print(area_polygon("cuadrado"))
print(area_polygon("triangulo"))
print(area_polygon("rectangulo"))
print(area_polygon("circulo"))