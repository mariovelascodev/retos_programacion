def reverse_string(string):
    #Recorremos la cadena a la inversa y cada letra la guardamos en otra variable
    new_string = ""
    
    for char in string[::-1]:
        new_string += char
    return new_string

print(reverse_string("Hola mundo"))